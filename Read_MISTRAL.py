# -*- coding: utf-8 -*-

# Hans Oosterbeek, 02.04.2020

# script to read MISTRAL NI data
# one requires package nptdms (Technical Data Management System) to read the NI-data files
# for instance from: https://anaconda.org/conda-forge/nptdms 


from nptdms import TdmsFile
import matplotlib.pyplot as plt



plt.close('all')

Pulse = '16_03_2020_2.tdms'

tdms_file = TdmsFile(Pulse)
tdms_groups = tdms_file.groups() # 0: Thermo-couples, 1: Analog channels, 2: Calculated channels

# For use with updated package nptdms installed with Anaconda and Spyder 4
tdms_Mistral_ADC_TC = tdms_groups[0].channels()
tdms_Mistral_ADC_AI = tdms_groups[1].channels()

# For use with installation at IPP computer(s)
# tdms_Mistral_ADC_TC = tdms_file.group_channels(tdms_groups[0])
# tdms_Mistral_ADC_AI = tdms_file.group_channels(tdms_groups[1])

# Read TC-channels. Allocation will vary per day. See also TC#.path, e.g. TC1.path
TC0 = tdms_Mistral_ADC_TC[0] # Bolometer #3,in V150R
TC1 = tdms_Mistral_ADC_TC[1] # Bolometer #7,opposite V150R
TC2 = tdms_Mistral_ADC_TC[2] # Port surface first cavity
TC3 = tdms_Mistral_ADC_TC[3] #
TC4 = tdms_Mistral_ADC_TC[4] # Temp. Kapton rear cavity QRB
# TC5 = tdms_Mistral_ADC_TC[5] # 
TC6 = tdms_Mistral_ADC_TC[6] # Bolo #10 middle cavity QRB
# TC7 = tdms_Mistral_ADC_TC[7] #
TC8 = tdms_Mistral_ADC_TC[8] # Bolo #6 first cavity QRB
# TC10 = tdms_Mistral_ADC_TC[10] #
TC11 = tdms_Mistral_ADC_TC[11] # Port surface middle cavity
TC12 = tdms_Mistral_ADC_TC[12] # Thermo-couple on Quartz window mounted on rear of QRB

# Read AI-channels
AI3 = tdms_Mistral_ADC_AI[3] # calorimeter power
AI4 = tdms_Mistral_ADC_AI[4] # ECRH power
AI5 = tdms_Mistral_ADC_AI[5] # HPT200

# Make title with day, pulse number and hour:minute
timestamp = TC0.properties['wf_start_time']
title_label = Pulse + ' @ ' + str(timestamp)[11:16]

# Plots

fig = plt.figure(1)
plt.plot(AI5.time_track(), AI5.data, label = AI5.path[28:-1])
plt.xlabel('Time [s]', fontsize = 12)
plt.ylabel('p [hPa]', fontsize = 12)
plt.ylim(0, 1E-3)
plt.title('Pressure # '+ title_label)
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.legend()
plt.grid()
plt.show()

fig = plt.figure(2)
fig.suptitle(title_label)
ax1 = plt.subplot(311)
plt.plot(AI4.time_track(), AI4.data/1000, label = AI4.path[28:-1])
plt.setp(ax1.get_xticklabels(), visible=False)
plt.ylabel('P kW]', fontsize = 12)
plt.ylim(-1, 12)
plt.legend(loc = 'upper right')
plt.grid()

ax2 = plt.subplot(312, sharex=ax1)
plt.plot(AI3.time_track(), AI3.data, label = AI3.path[28:-1])
plt.setp(ax2.get_xticklabels(), visible=False)
plt.ylabel('P [W]', fontsize = 12)
plt.ylim(-50, 300)
plt.legend(loc = 'upper right')
plt.grid()

ax3 = plt.subplot(313, sharex=ax1)
plt.plot(TC0.time_track(), TC0.data, label = TC0.path[28:-1])
#plt.setp(ax1.get_xticklabels(), visible=False)
plt.ylabel('T [° C]', fontsize = 12)
plt.ylim(0, 100)
plt.legend(loc='lower center')
plt.grid()

fig = plt.figure(3)
plt.plot(TC0.time_track(), TC0.data, label = TC0.path[28:-1])
plt.plot(TC1.time_track(), TC1.data, label = TC1.path[28:-1])
# plt.plot(TC2.time_track(), TC2.data, label = TC2.path[28:-1])
# plt.plot(TC3.time_track(), TC3.data, label = TC3.path[28:-1])
# plt.plot(TC4.time_track(), TC4.data, label = TC4.path[28:-1])
plt.plot(TC6.time_track(), TC6.data, label = TC6.path[28:-1])
# plt.plot(TC7.time_track(), TC7.data, label = TC7.path[28:-1])
plt.plot(TC8.time_track(), TC8.data, label = TC8.path[28:-1])
# plt.plot(TC10.time_track(), TC10.data, label = TC10.path[28:-1])
plt.xlabel('Time [s]', fontsize = 12)
plt.ylabel('Temperature [deg. C]', fontsize = 12)
# plt.ylim(15,60)
plt.title(title_label)
plt.legend()
plt.grid()
plt.show()