#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Hans Oosterbeek
Date: 01.04.2020

BTM_fit_eps
Code to find emssivity from MISTRAL graphite bolometers during the cool down phase
Input to the bolometer thermal model (BTM.py)

"""

from scipy.constants import pi
from scipy.constants import sigma as sig # Stefan-Boltzmann costant [W/(m^2K^4)]
import numpy as np
import matplotlib.pyplot as plt
from decimal import Decimal # to maintain acuracy of number if ends at zero

plt.close('all')

TK = 273 # zero degrees C in K


# Bolometer body data
eps = 0.66 # parameter to fit
r = 5E-3 # radius [m]
l = 10E-3 # length [m]
S = 2*pi*r**2 + 2*pi*r*l # Overall exposed surface [m^2]
V = pi*r**2*l # Volume [m^3]
rho = 1.8E3 # Specific density [kg/m^3] for 'R6810', 'Ringsdorff' DGL Carbon Group
m = rho * V # calculated from above: mass of the bolometer
# Coefficients for specific heat according to cp = a1*T + b1 (see BTM.py) 
a1 = 2.36; b1 = 4.75

# Experiment conditions
T_start = 311 # temperature at the moment the heating is switched off [deg. C]
T0 = T_start + TK
T_sur = 30 # Temperature [degree C] of walls surrounding the bolometer [deg. C]
Ts = T_sur + TK


# Timing vector for model
dt = 0.01 # 10 ms steps as in DAQ
T_C = 1500 # Time over which to compute the model data [s]
N = int(T_C/dt) # number of points to calculate
t = np.linspace(0, N-1, N) # Time vector in dt incremenst
ts = tuple([i*dt for i in t]) # Time vector in s incremenst


# Plot of experiment data
DAQ_name = '20200317_10_B7.npy'
DAQ = np.load(DAQ_name)
DAQ_size = np.size(DAQ)
dt_d = 0.01 # [s] Sample period time data ADC, set by hand (for now). Compute or from data.properties[wf-increment] in the tdms-file
t_DAQ = np.linspace(0, DAQ_size*dt_d, DAQ_size)
fig = plt.figure(1)
plt.plot(t_DAQ, DAQ)
plt.xlabel('t [s]')
plt.ylabel('T [$^{\circ}$C]')
plt.title('Bolometer signal for: '+ DAQ_name)
plt.grid()


# Model the cooling down phase: surface radiated of body with mass m and cp = f(T)
T_cool = np.zeros(N) # array with temperature
T = T0
T_cool[0] = T
i = 1
while i < N:
 	num = -1*eps*sig*S*(T**4-Ts**4)
 	den = (a1*T+b1)*m
 	dT = (num/den)*dt
 	T = T + dT
 	T_cool[i] = T
 	i = i +1
T_cool_C = tuple([n-TK for n in T_cool]) # convert array to degrees C
eps_dec = Decimal(eps).quantize(Decimal("1.00")) # to also get x.00 for an eps with two dec. zeros
fig = plt.figure(2)
plt.plot(ts,T_cool_C, label = 'Model with $\epsilon$ = '+ str(eps_dec))

# Add the experiment data to the plot
t_start = 1813 # find by hand the start time of the cooling
N_start = int(DAQ_size*(t_start/(DAQ_size*dt_d)))
fig = plt.figure(2)
plt.plot(t_DAQ[0:DAQ_size-N_start-1], DAQ[N_start:-1], label = 'Data: ' + DAQ_name)
plt.xlabel('t [s]')
plt.ylabel('T [$^{\circ}$C]')
plt.legend(loc='upper right')
plt.grid()
plt.title('Bolometer cooling down: fit emissivity to data')