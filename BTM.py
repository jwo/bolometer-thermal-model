#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Hans Oosterbeek
Date: 01.04.2020

BTM.py (Bolometer Thermal Model)
Thermal model of graphite bolometers used in the MISTRAL.
Temperature variation by absorption of microwaves (source) and radiation (sink)
Specific Heat Capacity and Stray radiation absorption coefficient are optionally temperature dependent

Input (fit) parameters:
eps: Emissivity of bolometer. Determine with code BTM_fit_eps. Typically 0.66 (but literature 0.81)
A: Microwave Stray Radiation absorption coefficient at room temp of bolometer. Typically 0.074
p: Power density at location of bolometer [W/m^2]

Note that - if available - the equilibrium temperature provides product pA: use code BTM_eq.

Room temperature at 293 K


"""

from scipy.constants import pi
from scipy.constants import sigma as sig # Stefan-Boltzmann costant [W/(m^2K^4)]
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')


## constants
TK = 273 # zero degrees C in K


## Temperatures
T_start = 33.5 # start temperature of body in deg. C. Obtain from an initial data plot
T0 = TK + T_start # T_start in K
T_sur = 30 # Temperature [degree C] of walls surrounding the bolometer (vessel or port)
Ts = TK + T_sur # T_sur in K


## Fitparameters
eps = 0.66 # Emissivity [-]. Obtain from fit of cooling down phase
A = 0.074 # Microwave stray radiation absoption coefficient at room temperature
# pA = 4.283E3 # (power density) * (absorption coefficient) in case an equilibrium temperature is available.
# Note that in case A = f(T), one must use A at equilibrium temperature to find p from pA 
p = 56.5E3 # power density as input (fit) parameter.
pd = p # pd in order to print p at the end of the code (p is set to zero in the heating-off phase)


## Bolometer body data
r = 5E-3 # radius [m]
l = 10E-3 # length [m]
S = 2*pi*r**2 + 2*pi*r*l # Overall exposed surface [m^2]
V = pi*r**2*l # Volume [m^3]
cp = 715 # Specific Heat Capacity [J/(kg*K)] at room temperature
rho = 1.8E3 # Specific density [kg/m^3] for 'R6810', 'Ringsdorff' DGL Carbon Group
m = rho * V # calculated from above: mass of the bolometer


## Data to fit the model to  
DAQ_name = '20200317_10_B7.npy'


## Time vector and heating on/ off phase (to match DAQ data)
dt = 0.01 # 10 ms steps increament in model
H_on = 1812 # Duration of heating on-time. Find from plot of data
H_off = 1688 # Duration of heating off-time. Find from plot of data
N = int(H_on/dt) # number of points to calculate during heating on
M = int(H_off/dt) # number of points to calculate after the heating on
t = np.linspace(0, N+M-1, N+M) # Time vector in dt incremenst
ts = tuple([i*dt for i in t]) # Time vector in s incremenst


## Specific Heat Capacity = f(T)
# Data from Handbook of Chemistry and Physics", 84th Edition, 2002-2003, p 12-190
# Material used for bolometer: 'R6810',Ringsdorff, SGL Carbon Group
cp_K = (200, 250, 300, 350, 400, 500, 600)
cp_T = tuple([417.5, 568.3, 715.0, 853.3, 984.2, 1218.3, 1403.0])
fig = plt.figure(1)
cp_C = tuple([i-273 for i in cp_K]) # convert array to degrees C
plt.plot(cp_C, cp_T, '*', label = 'Literature')
# rough fit of cp_T
T_0 = cp_K[0]
cp_0 = cp_T[0]
T_6 = cp_K[6]
cp_6 = cp_T[6]
offset_a = -0.1 # add offset by eye to improve fit (basically: optimise for room temp. to max. temp)
offset_b = 20 # add offset by eye to improve fit (basically: optimise for room temp. to max. temp)
a1 = (cp_6-cp_0)/(T_6-T_0) + offset_a
b1 = cp_6 - a1*T_6 + offset_b
cp_x = np.linspace(200, 600, 401)
cp_y = cp_x*a1+b1
cp_xC = tuple([i-273 for i in cp_x]) # # convert array to degrees C
plt.plot(cp_xC, cp_y, label = 'Fit c$_{p}$ = a1*T + b1')
plt.xlabel('T [$^{\circ}$C]')
plt.ylabel('c$_{p}$ [J/(kg K)s]')
plt.grid()
plt.legend()
plt.title('Bolometer Specific Heat Capacity as function of T')
plt.show()

# To switch temperature dependence c_p off
# a1 = 0 # Temperature coefficient zero
# b1 = cp # c_p at room temperture


## Stray radiation coefficient A = f(T)
# Paper by Shawn E. Bourdo shows 25 % increase in electrica conductivity from T_room to 
# 300 degrees C for their type of graphite (Journal of Solid State Chemistry 196 (2012) 309–313)
# For the microwave absorbed fraction this leads to a decrease of 10 % of the 
# absorbed fraction (Surface Resistance, Goldsmith 5.95)
# Data for our ACTUAL material ('R6810', 'Ringsdorff') of course preferred
A_20 = (20, A)
A_300 = (300, 0.90*A)
a2 = ( (A_300[1]-A_20[1]) / (A_300[0]-A_20[0]) )
b2 = A_300[1]-a2*A_300[0]
A_x = np.linspace(0, 350, 351)
A_T = a2*A_x+b2
fig = plt.figure(2)
plt.plot(A_x, A_T, label = 'Fit ' + r'$\alpha$ = a2*T + b2')
plt.xlabel('T [$^{\circ}$C]')
plt.ylabel(r'$\alpha$ [-]')
plt.grid()
plt.legend()
plt.title('Bolometer absorbed fraction as function of T')
plt.show()

# To switch temperature dependence alpha off
a2 = 0 # Temperature coefficient zero
b2 = A # alpha room temperture


## Compute delta T, heating on-time
# eps = 0 # Optional: no radiation
T = T0  # start temperature
T_b_on = np.zeros(N) # array with bolometer temperature during heating on time
T_b_on[0] = T
i = 1  
while i < N:
 	num = (a2*T+b2)*p*S - eps*sig*S*(T**4-Ts**4)
 	den = (a1*T+b1)*m	
 	dT = (num/den)*dt
 	T = T + dT
 	T_b_on[i] = T
 	i = i +1

## Compute delta T, heating off-time
p = 0 
T_start = T_b_on[int((H_on/dt) -1)] # temperature as heating  stopped
T = T_start
T_b_off = np.zeros(M) ## array with bolometer temperature during heating off time
T_b_off[0] = T
i = 1  
while i < M:
 	num = (a2*T+b2)*p*S - eps*sig*S*(T**4-Ts**4)
 	den = (a1*T+b1)*m	
 	dT = (num/den)*dt
 	T = T + dT
 	T_b_off[i] = T
 	i = i +1

T_bK = np.append(T_b_on, T_b_off) # Bolometer temperature during heating on + off in K
T_b = tuple([i-273 for i in T_bK]) # convert array to degrees C
fig = plt.figure(3)
plt.plot(ts,T_b, label = 'Bolometer thermal model')


## compare to experiment data
DAQ = np.load(DAQ_name)
DAQ_size = np.size(DAQ)
dt_d = 0.01 # [s] Sample period time data ADC, set by hand (for now). Compute or from data.properties[wf-increment] in the tdms-file
t_DAQ = np.linspace(0, DAQ_size*dt_d, DAQ_size)
leg_str = ('Data: ' + DAQ_name)
plt.plot(t_DAQ, DAQ, label = leg_str)
plt.xlim(-50, (N+M)*dt+50)
plt.ylim(0, 360)
plt.xlabel('t [s]')
plt.ylabel('T [$^{\circ}$C]')
plt.legend(loc='lower center')
plt.grid()
t_str = ('Thermal model fitted to data: obtain p [kW/m^2]')
plt.title(t_str)
p = int(pd/1000)
plt_str = r'$\alpha$ = '+str(A)+', $\epsilon$ = ' + str(eps) + ' (p = ' + str(p) + ' kWm$^{-2}$)'
plt.text(500, 330, plt_str)
plt.show()