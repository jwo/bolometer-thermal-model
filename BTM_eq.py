#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Hans Oosterbeek
Date: 01.04.2020

BTM_eq
Code to find the product of (power density) * (absorption coefficient bolometer)
in case the bolometer is at equilibrium temperature.
Input to Thermal Model Bolometer (BTM.py)

"""

from scipy.constants import sigma as sig # Stefan-Boltzmann costant [W/(m^2K^4)]
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

TK = 273 # for conversion Kelvin to degrees C


# Experiment conditions
T_sur = 30 # Temperature [degree C] of walls surrounding the bolometer (vessel or port)
Ts = T_sur + TK

# Bolometer data
eps = 0.67 # From BTM_fit_eps


# Plot of experiment data
DAQ_name = '20200317_10_B7.npy'
DAQ = np.load(DAQ_name)
DAQ_size = np.size(DAQ)
dt_d = 0.1 # [s] Sample period time data ADC, set by hand (for now). Compute or from data.properties[wf-increment] in the tdms-file
t_DAQ = np.linspace(0, DAQ_size*dt_d, DAQ_size)
fig = plt.figure(1)
plt.plot(t_DAQ, DAQ)
plt.xlabel('t [s]')
plt.ylabel('T [$^{\circ}$C]')
plt.title('Bolometer signal for: '+ DAQ_name)
plt.grid()

## Read the bolometer equilibrium temperature and calculate pA
T_eqC = 317 # equilibrium temperature in degrees C by hand
T_eq = T_eqC +  TK # equilibrium temperature in K

## Calculate pA in equilibrium
pA = eps*sig*(T_eq**4-Ts**4)

## Add data to plot
T_eq_C = T_eq - TK
pAr = np.round(pA/1000, decimals = 3)
plt_str = 'T$_{eq}$ = ' + str(T_eq_C) + ' $^{\circ}$C'
plt.text(250, 170, plt_str)

pAr = np.round(pA/1000, decimals = 3)
plt_str = r'p*$\alpha$ = ' + str(pAr) + ' kWm$^{-2}$'
plt.text(250, 140, plt_str)

# Just for interest
S = 0.00047 # bolometer exposed surface [m^2], see BTM.px
b_p = np.round(S*pA, decimals = 2)
plt_str = '(Bolometer power = ' + str(b_p) + ' W)'
plt.text(250, 110, plt_str)

